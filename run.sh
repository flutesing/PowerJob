#!/usr/bin/env bash
# 定义应用组名
group_name='flutesing'
# 定义应用名称
app_name='powerjob-server'
# 定义应用版本
app_version='latest'
# 定义应用环境
profile_active='prod'
echo '----copy jar----'
docker stop ${app_name}
echo '----stop container----'
docker rm ${app_name}
echo '----rm container----'
docker rmi ${group_name}/${app_name}:${app_version}
echo '----rm image----'
# 打包编译docker镜像
docker build -t ${group_name}/${app_name}:${app_version} .
echo '----build image----'
docker run -p 8901:7700 -p 8902:10086 -p 8903:10010 --name ${app_name} \
-e PARAMS: "--oms.mongodb.enable=false --spring.datasource.core.jdbc-url=jdbc:mysql://sh-cynosdbmysql-grp-61mx50k0.sql.tencentcdb.com:26064/powerjob?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai --spring.datasource.core.username=sailor --spring.datasource.core.password=BW9Ah0MA" \
-v /etc/localtime:/etc/localtime \
-d  --restart=always  ${group_name}/${app_name}:${app_version}
echo '----start container----'

